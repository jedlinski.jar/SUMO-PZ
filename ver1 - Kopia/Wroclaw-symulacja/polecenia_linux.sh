xmlstarlet ed -d "/osm/*[@action='delete']" < worker.osm > wroclaw.osm

netconvert --osm-files wroclaw.osm -o wroclaw.net.xml --no-turnarounds

polyconvert --net-file wroclaw.net.xml --osm-files wroclaw.osm --type-file typemap.xml -o wroclaw.poly.xml

python /usr/share/sumo/tools/randomTrips.py -n wroclaw.net.xml --seed 42 --fringe-factor 5 -p 1.42638308523 -r wroclaw.passenger.rou.xml -o wroclaw.passenger.trips.xml -e 3600 --vehicle-class passenger --vclass passenger --prefix veh --min-distance 300 --trip-attributes departLane=\"best\" --validate

python /usr/share/sumo/tools/randomTrips.py -n wroclaw.net.xml --seed 42 --fringe-factor 20 -p34.8020507688 -r wroclaw.tram.rou.xml -o wroclaw.tram.trips.xml -e 3600 --vehicle-class tram --vclass tram --prefix tram --min-distance 1200 --trip-attributes departLane=\"best\" --validate

sumo-gui -c map.sumocfg --additional-files detectors.xml

