# -*- coding: utf-8 -*-
import os
import sys
import random

import subprocess
import traci
import traci.constants as tc

INFINITY = 1e400

PORT = 8884
SUMO_HOME = os.path.realpath(os.environ.get(
    "SUMO_HOME", os.path.join(os.path.dirname(__file__), "..", "..", "..", "..")))
sys.path.append(os.path.join(SUMO_HOME, "tools"))
try:
    from sumolib import checkBinary
except ImportError:
    def checkBinary(name):
        return name
NETCONVERT = checkBinary("netconvert")
SUMO = checkBinary("sumo")
SUMOGUI = checkBinary("sumo-gui")

sumoExe = SUMOGUI
sumoConfig = "map.sumocfg"
sumoProcess = subprocess.Popen([sumoExe, sumoConfig], stdout=sys.stdout, stderr=sys.stderr)
traci.init(PORT)
traci.simulation.subscribe()

step = 0

while step < 36000:
	traci.simulationStep()
	
	departed = traci.simulation.getSubscriptionResults()[tc.VAR_DEPARTED_VEHICLES_IDS]

	for v in departed:
		traci.vehicle.subscribe(v)
		subs = traci.vehicle.getSubscriptionResults(v)
		print("id " + str(subs[tc.VAR_ROAD_ID]) + " lane " + str(subs[tc.VAR_LANEPOSITION]) + " dest " )
		
		if 0 == random.randint(0, 1):
			print "przekierowanie"
			try:
				traci.vehicle.changeTarget(v, "28458116#2")
			except:
				pass
		
	step+=1
	
traci.close()
sumoProcess.wait()