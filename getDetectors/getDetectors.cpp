// getDetectors.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"
#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace rapidxml;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc > 1){
		try{
			file<> xmlFile(argv[1]);

			xml_document<> doc;

			try {
				doc.parse<0>(xmlFile.data());
			}
			catch (parse_error p) {
				return false;
			}

			xml_node <> *root = doc.first_node();

			std::fstream file;
			file.open("detectors.txt", std::ios::trunc | std::ios::out);

			if (file.good()){
				for (xml_node <> * node = root->first_node(); node; node = node->next_sibling()) {
					file << node->first_attribute()->value() << "\n";
				}
			}

			file.close();
			doc.clear();
		}
		catch (...){
			std::cout << "Its an error\n";
		}
		std::cout << "No file name specified\n";
	}

	return 0;
}

