netconvert --osm-files wroclaw.osm -o wroclaw.net.xml
polyconvert --net-file wroclaw.net.xml --osm-files wroclaw.osm --type-file typemap.xml -o wroclaw.poly.xml
python E:\SUMO\tools\randomTrips.py -n wroclaw.net.xml -r wroclaw.rou.xml -e 5000 -l
sumo-gui -c wroclaw.sumo.cfg