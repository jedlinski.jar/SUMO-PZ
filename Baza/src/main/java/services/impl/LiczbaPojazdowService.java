package services.impl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import connectors.DbHelper;

public class LiczbaPojazdowService {

	public void selectHours(){
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream("godziny - all.txt"), "utf-8"))){
			
			String query = "SELECT DISTINCT Czas_Poczatku FROM DPTLiczba_Pojazdow_Pomiar ORDER BY Czas_Poczatku ASC" ;
			
			try (Connection connection = DbHelper.getConnection();
					PreparedStatement pstmt = connection.prepareStatement(query)) {

				ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");
					System.out.println(String.valueOf(formatter.format(rs.getTimestamp("Czas_Poczatku"))));
					writer.write(formatter.format(rs.getTimestamp("Czas_Poczatku")) + "\n");
				}
				
				writer.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String selectIts(String czasPocz, int nrSkrzyzowania, int nrDetektora) throws SQLException {

		String query = "SELECT Liczba_Pojazdow FROM DPTLiczba_Pojazdow_Pomiar WHERE Czas_Poczatku = '"
				+ czasPocz + "' AND Nr_Skrzyzowania = " + nrSkrzyzowania
				+ " AND Nr_Detektora_Licz = " + nrDetektora;

		try (Connection connection = DbHelper.getConnection();
				PreparedStatement pstmt = connection.prepareStatement(query)) {

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {

				System.out.println(String.valueOf(rs.getInt("Liczba_Pojazdow")));
				return String.valueOf(rs.getInt("Liczba_Pojazdow"));

			}
		}
		System.out.println("1");
		return "1";
	}
}
