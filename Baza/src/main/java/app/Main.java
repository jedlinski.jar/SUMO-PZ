package app;

import static java.lang.Integer.valueOf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;

import javax.swing.JFrame;

import connectors.DbHelper;
import services.impl.LiczbaPojazdowService;

public class Main extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static int nrSkrzyzowania(String detektor) {
		String[] result = detektor.split("-");
		return valueOf(result[1].split("V")[0]);
	}

	private static int nrDetektora(String detektor) {
		String[] result = detektor.split("-");
		return valueOf(result[0].substring(1));
	}

	public static void main(String[] args) throws SQLException {
		DbHelper.getInstance().init();

		LiczbaPojazdowService service = new LiczbaPojazdowService();
		
		if (args[0].equals("its")){
			BufferedReader detectors = null;
			BufferedReader godziny = null;
	
			try {
				godziny = new BufferedReader(new FileReader(new File("godziny.txt")));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
	
			try {
				int hr = 0;
				for (String godzina; (godzina = godziny.readLine()) != null;) {
	
					try (Writer writer = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(godzina.replace(":", "-").replace(" ", "") + ".txt"), "utf-8"))) {
	
						detectors = new BufferedReader(new FileReader(new File("detectors.txt")));
		
						writer.write("Detector;Time;qPKW;qLKW;vPKW;vLKW\n");
						
						for (String detektor; (detektor = detectors.readLine()) != null;) {
							writer.write(detektor + ";" + hr + ";" 
									+ service.selectIts(godzina, nrSkrzyzowania(detektor), nrDetektora(detektor)) + ";0;10.000;0");
							writer.write("\n");
						}
						
						writer.close();
					}
					hr += Integer.parseInt(args[1]);
				}
			} catch (IOException e) {
				System.out.println("NIe udalo sie ktoregos pliku otworzyc. Motyla noga!");
				e.printStackTrace();
			}
		}else if (args[0].equals("hours")){
			service.selectHours();
		}
	}
}
