package connectors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class DefaultStatementExecutor {

	public boolean execute(String statement) throws SQLException {
		String sql = statement;
		try (Connection connection = DbHelper.getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql)) {
			return pstmt.execute();
		}
	}

	public String executeSelect(String statement) throws SQLException {
		String result = "";
		String sql = statement;
		int columnsNumber = 0;

		try (Connection connection = DbHelper.getConnection();
				PreparedStatement pstmt = connection.prepareStatement(sql)) {
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();

			System.out.println("querying " + statement);
			columnsNumber = rsmd.getColumnCount();
			for(int i = 1; i<= columnsNumber; i++){
				result += String.format("%17s|", rsmd.getColumnName(i));
				System.out.print(String.format("%17s|", rsmd.getColumnName(i)));
			}
			result += "\n";
			System.out.println("");
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					String columnValue = rs.getString(i);
					result += String.format("%17s|", columnValue );
					System.out.print(String.format("%17s|", columnValue ));
				}
				result += "\n";
				System.out.println("");

			}
			System.out.println("");
			return result;
		}
	}
}
