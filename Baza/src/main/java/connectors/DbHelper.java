package connectors;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;

public class DbHelper {

	private static final DbHelper INSTANCE = new DbHelper();
	
	private DbHelper(){}
	
	public static DbHelper getInstance(){
		return DbHelper.INSTANCE;
	}
	
	private BasicDataSource ds;
	
	public void init(){
		System.out.println("Tworzenie Datasource");
		ds = new BasicDataSource();
		ds.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		ds.setUrl("jdbc:sqlserver://localhost:1433;databaseName=ITS;integratedSecurity=false");
		ds.setUsername("johny");
		ds.setPassword("johny1234");
	}
	
	public void close(){
		if(ds != null){
			try {
				System.out.println("Zamykanie Datasource");
				ds.close();
			} catch (SQLException e) {
				System.out.println("Wystapil blad podczas zamykania Datasource");
				e.printStackTrace();
			}
		}
	}

	public BasicDataSource getDs() {
		return ds;
	}
	
	public static Connection getConnection() throws SQLException{
	
		return getInstance().getDs().getConnection();
		
	}
}
